﻿namespace Exercism.anagram
{
    using System.Collections.Generic;
    using System.Linq;

    class Anagram
    {
        private string anagram;

        public Anagram(string anagram)
        {
            this.anagram = anagram;
        }

        public string[] Match(string[] words)
        {
            var result = new List<string>();
            foreach (var word in words)
            {
                if (word.ToLower().Length != anagram.Length || word.ToLower() == anagram)
                {
                    continue;
                }
                var charsToMatch = anagram.ToLower();
                foreach (var s in word.ToLower().ToCharArray())
                {
                    if (charsToMatch.Contains(s.ToString()))
                    {
                        var index = charsToMatch.IndexOf(s);
                        charsToMatch = charsToMatch.Remove(index, 1);
                    }
                }
                if (charsToMatch.Length == 0)
                {
                    result.Add(word);
                }
            }
            return result.ToArray();
        }
    }
}
