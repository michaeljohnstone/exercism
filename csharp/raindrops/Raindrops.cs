﻿using System;
using System.Linq;

internal class Raindrops
{
    private static readonly Tuple<int, string>[] _sounds = {
        Tuple.Create(3, "Pling"),
        Tuple.Create(5, "Plang"),
        Tuple.Create(7, "Plong")
    };
    public static string Convert(int number)
    {
        return
            string.Concat(_sounds.Where(x => number%x.Item1 == 0)
            .Select(x => x.Item2)
            .DefaultIfEmpty(number.ToString()));
    }
}