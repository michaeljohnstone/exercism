﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

public enum NumberType
{
    Deficient,
    Perfect,
    Abundant
}

public class PerfectNumbers
{
    public static NumberType Classify(int number)
    {
        var factors = Factorise(number);
        var sum = factors.Sum();
        if (factors.Length == 1) //it is prime
            return NumberType.Deficient;
        if (sum == number)
            return NumberType.Perfect;

        return NumberType.Abundant;
    }

    private static int[] Factorise(int number)
    {
        var factors = new List<int> {1}; //1 is always a factor
        
        for (var i = 2; i < Math.Ceiling(number/2.0); i++)
        {
            if (number%i == 0 && !factors.Exists(x => x == i))
            {
                factors.Add(i);
                factors.Add(number/i);
            }
        }
        return factors.ToArray();
    }

}