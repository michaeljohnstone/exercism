﻿using System.Collections.Generic;
using System.Linq;

internal class Garden
{
    private readonly List<string> _students;
    private readonly string _plants;

    private static readonly string[] _defaultStudents = {"Alice", "Bob", "Charlie", "David", "Eve", "Fred", "Ginny", "Harriet", "Ileana", "Joseph", "Kincaid", "Larry"};
    public Garden(string[] students, string plants)
    {
        _students = students.ToList();
        _plants = plants;
        _students.Sort();
    }

    public static Garden DefaultGarden(string plants)
    {
        return new Garden(_defaultStudents, plants);
    }

    public Plant[] GetPlants(string student)
    {
        var index = _students.IndexOf(student);
        
        var plants = new List<Plant>();
        if (index != -1)
        {
            var rows = _plants.Split('\n');
            plants.Add((Plant)rows[0][index*2]);
            plants.Add((Plant)rows[0][index*2 + 1]);
            plants.Add((Plant)rows[1][index*2]);
            plants.Add((Plant)rows[1][index*2 + 1]);
        }
        return plants.ToArray();
    }
}

internal enum Plant
{
    Radishes = 'R',

    Clover = 'C',

    Grass = 'G',

    Violets = 'V'
}