﻿using System.Collections.Generic;

internal class PrimeFactors
{
    public static int[] For(long i)
    {
        var factors = new List<int>();
        var remaining = i;
        var potentialFactor = 2;
        while (remaining > 1)
        {
            while (remaining % potentialFactor == 0)
            {
                factors.Add(potentialFactor);
                remaining /= potentialFactor;
            }
            potentialFactor += potentialFactor == 2 ? 1 : 2;
        }
        return factors.ToArray();
    }    
}