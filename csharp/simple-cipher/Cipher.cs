﻿using System;
using System.Text.RegularExpressions;

internal class Cipher
{

    public Cipher(string key)
    {
        var match = Regex.Match(key, "^[a-z]+$");
        if (!match.Success)
        {
            throw new ArgumentException();
        }
        Key = key;
    }

    public Cipher()
    {
        Key = GenerateKey();
    }

    public string Key { get; set; }

    public string Encode(string toEncode)
    {
        var encoded = string.Empty;
        for (var i = 0; i < toEncode.Length; i++)
        {
            encoded += (char)((toEncode[i] + Key[i] - 'a' - 'a') % 26 + 'a');
        }
        return encoded;
    }

    public object Decode(string toDecode)
    {
        var decoded = string.Empty;
        for (var i = 0; i < toDecode.Length; i++)
        {
            decoded += (char)(toDecode[i] -Key[i] + 'a');
        }
        return decoded;
    }

    private static string GenerateKey()
    {
        const string Chars = "abcdefghijklmnopqrstuvwxyz";
        const int KeyLength = 100;
        var randomString = string.Empty;
        var random = new Random();
        for (var i = 0; i < KeyLength; i++)
        {
           randomString +=  Chars[random.Next(Chars.Length)];
        }
        return randomString;
    }
}