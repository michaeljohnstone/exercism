﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

internal class Trinary
{
    public static int ToDecimal(string value)
    {
        if (Regex.IsMatch(value, "^[0-2]+$"))
        {
            return value.Select((t, i) => int.Parse(value.Substring(i, 1))*(int)Math.Pow(3, value.Length - i - 1)).Sum();
        }
        return 0;
    }
}