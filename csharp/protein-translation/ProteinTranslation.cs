﻿using System;
using System.Collections.Generic;
using System.Linq;

internal class ProteinTranslation
{
    private static Dictionary<List<string>, string> codonsToProteins = new Dictionary<List<string>, string>()
    {
        { new List<string>() {"AUG"}, "Methionine"},
        { new List<string>() {"UUU", "UUC"}, "Phenylalanine"},
        { new List<string>() {"UUA", "UUG"}, "Leucine"},
        { new List<string>() {"UCU", "UCC", "UCA", "UCG"}, "Serine"},
        { new List<string>() {"UAU", "UAC"}, "Tyrosine"},
        { new List<string>() {"UGU", "UGC"}, "Cysteine"},
        { new List<string>() {"UGG"}, "Tryptophan"},
        { new List<string>() {"UAA", "UAG", "UGA"}, "STOP"}
    };

    public static string[] Translate(string codon)
    {
        var codons = Split(codon, 3);
        var proteins = new List<string>();
        var codonsFound = 0;
        foreach (var item in codons.SelectMany(c => codonsToProteins.Where(item => item.Key.Any(codonKey => c == codonKey))))
        {
            if (item.Value == "STOP")
            {
                return proteins.ToArray();
            }
            proteins.Add(item.Value);
            codonsFound++;
        }
        if (codonsFound != codons.Length)
        {
            throw new Exception();
        }
        return proteins.ToArray();
    }

    public static string[] Split(string str, int noChars)
    {
        return Enumerable.Range(0, str.Length / noChars).Select(i => str.Substring(i * noChars, noChars)).ToArray();
    }
}