﻿using System;
using System.Collections.Generic;

internal class TwelveDaysSong
{
    private const string startOfSong = "On the {0} day of Christmas my true love gave to me";

    private string[] verses = new string[12]
    {
        "and a Partridge in a Pear Tree",
        "two Turtle Doves",
        "three French Hens",
        "four Calling Birds",
        "five Gold Rings",
        "six Geese-a-Laying",
        "seven Swans-a-Swimming",
        "eight Maids-a-Milking",
        "nine Ladies Dancing",
        "ten Lords-a-Leaping",
        "eleven Pipers Piping",
        "twelve Drummers Drumming"

    };
        

    private const string verseSeparator = ", ";

    private const string endOfSong = ".\n";
    public string Verse(int verseNumber)
    {
        var song = string.Format(startOfSong, GetDayAsOrdinalWord(verseNumber));
        if (verseNumber == 1)
        {
            return song + verseSeparator + verses[verseNumber - 1].Substring(4, verses[verseNumber - 1].Length-4) + endOfSong;
        }
        for (var i = verseNumber; i > 0; i--)
        {
            song += verseSeparator + verses[i - 1];
        }

        return song + endOfSong;
    }

    /// <summary>
    /// Convert an integer from the range 1-12 to it's ordinal word
    /// </summary>
    /// <param name="i">The integer to convert</param>
    /// <returns>The integer as an ordinal word</returns>
    private string GetDayAsOrdinalWord(int i)
    {
        switch (i)
        {
            case 1:
                return "first";
            case 2:
                return "second";
            case 3:
                return "third";
            case 4:
                return "fourth";
            case 5:
                return "fifth";
            case 6:
                return "sixth";
            case 7:
                return "seventh";
            case 8:
                return "eighth";
            case 9:
                return "ninth";
            case 10:
                return "tenth";
            case 11:
                return "eleventh";
            case 12:
                return "twelfth";
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public string Verses(int from, int to)
    {
        var song = string.Empty;
        for (var i = from; i <= to; i++)
        {
            song += Verse(i) + "\n";
        }
        return song;

    }

    public string Sing()
    {
        return Verses(1,12);
    }
}