﻿using System.Collections.Generic;
using System.Linq;

internal class Complement
{

    private static readonly Dictionary<char, char> _complements = new Dictionary<char, char>()
    {
        {'C','G' },
        {'G','C' },
        {'T','A' },
        {'A','U' }
    }; 
    public static string OfDna(string s)
    {
        return string.Concat(s.Select(x => _complements[x]));
    }
}