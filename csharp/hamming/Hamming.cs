﻿namespace Exercism.hamming
{
    using System.Linq;

    class Hamming
    {
        public static int Compute(string strandOne, string strandTwo)
        {
            return strandOne.Where((t, i) => t != strandTwo[i]).Count();
        }
    }
}
