﻿using System;
using System.Collections.Generic;
using System.Linq;

internal static class Strain
{
    public static IEnumerable<T> Keep<T>(this IEnumerable<T> enumerable, Func<T, bool> pred)
    {
        var toKeep = new List<T>();
        foreach (var item in enumerable)
        {
            if (pred(item))
            {
                toKeep.Add(item);
            }
        }
        return toKeep;
    }

    public static IEnumerable<T> Discard<T>(this IEnumerable<T> enumerable, Func<T, bool> pred)
    {
        return enumerable.Keep(x => !pred(x));
    }

}

