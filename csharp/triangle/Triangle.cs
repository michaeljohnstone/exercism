﻿using System;

internal class Triangle
{
    public static TriangleKind Kind(decimal sideOne, decimal sideTwo, decimal sideThree)
    {
        if (!IsTriangleLegal(sideOne, sideTwo, sideThree))
            throw new TriangleException();
        if (IsEquilateral(sideOne, sideTwo, sideThree))
            return TriangleKind.Equilateral;
        if (IsIsosceles(sideOne, sideTwo, sideThree))
            return TriangleKind.Isosceles;
        if (IsScalene(sideOne, sideTwo, sideThree))
            return TriangleKind.Scalene;

        throw new TriangleException();
    }

    private static bool IsIsosceles(decimal sideOne, decimal sideTwo, decimal sideThree)
    {
        return sideOne == sideTwo && sideTwo != sideThree || sideTwo == sideThree && sideThree != sideOne || sideThree == sideOne && sideOne != sideTwo;
    }

    private static bool IsEquilateral(decimal sideOne, decimal sideTwo, decimal sideThree)
    {
        return sideOne == sideTwo && sideTwo == sideThree;
    }

    private static bool IsScalene(decimal sideOne, decimal sideTwo, decimal sideThree)
    {
        return sideOne != sideTwo && sideTwo != sideThree && sideThree != sideOne;
    }

    private static bool IsTriangleLegal(decimal sideOne, decimal sideTwo, decimal sideThree)
    {
        //check all sides greater than 0
        return (sideOne > 0 && sideTwo > 0 && sideThree > 0) &&
            //check triangle inequality principle
            (sideOne + sideTwo > sideThree && sideTwo + sideThree > sideOne && sideThree + sideOne > sideTwo);
    }
}

public enum TriangleKind
{
    Equilateral,
    Isosceles,
    Scalene
}

public class TriangleException : Exception
{
    
}