﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text.RegularExpressions;
using NUnit.Framework.Constraints;

internal class Crypto
{
    private int _size;

    private readonly int _maxNumberOfCharsPerSegment;

    public Crypto(string text)
    {
        NormalizePlaintext = Regex.Replace(text, "[^a-zA-Z0-9]", "").ToLower();
        _maxNumberOfCharsPerSegment = (int) Math.Ceiling((double)NormalizePlaintext.Length/Size);
    }

    public string NormalizePlaintext { get; }

    public int Size
    {
        get
        {
            if (_size == 0)
            {
                _size = (int)Math.Ceiling(Math.Sqrt(NormalizePlaintext.Length));
            }
            return _size;
        }   
    }

    public string[] PlaintextSegments()
    {
        var segments = new List<string>();
        for (var i = 0; i < _maxNumberOfCharsPerSegment; i++)
        {
            var segmentLength = Size < NormalizePlaintext.Length - Size*i ? Size : NormalizePlaintext.Length - Size*i;
            var segment = NormalizePlaintext.Substring(Size*i, segmentLength);
            segments.Add(segment);
         
        }
        return segments.ToArray();
    }

    public string Ciphertext()
    {
        return NormalizeCiphertext().Replace(" ", "");
    }

    public string NormalizeCiphertext()
    {
        var plainTextSegments = PlaintextSegments();
        var ciphered = string.Empty;

        for (var i = 0; i < plainTextSegments[0].Length; i++)
        {
            ciphered = plainTextSegments.Where(s => i < s.Length).Aggregate(ciphered, (current, s) => current + s[i]);
            ciphered += " ";
        }
        return ciphered.Trim();
    }
}
