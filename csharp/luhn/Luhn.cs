﻿using System;
using System.Linq;

internal class Luhn
{
    private readonly long _luhn;

    internal int[] Addends
    {
        get
        {
            var temp = _luhn.ToString().Select(digit => (int) char.GetNumericValue(digit)).ToArray();
            for (var i = temp.Length-2; i >= 0; i -= 2)
            {
                temp[i] *= 2;
                if (temp[i] > 9)
                {
                    temp[i] -= 9;
                }
            }
            return temp;
        }
    }

    internal long CheckDigit => _luhn % 10;

    internal int Checksum => Addends.Sum();

    internal bool Valid => Checksum%10 == 0;


    public Luhn(long luhn)
    {
        _luhn = luhn;
    }

    public static long Create(long i)
    {
        var zeroAppended = long.Parse(i + "0");
        var luhn = new Luhn(zeroAppended);
        var checkSum = luhn.Checksum;
        if (checkSum %10 == 0 )
            return zeroAppended;
        var extraDigitRequired = 10 - checkSum %10 ;

        return long.Parse(i + extraDigitRequired.ToString());
    }
}