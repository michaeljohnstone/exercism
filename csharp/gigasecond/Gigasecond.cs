﻿using System;

internal class Gigasecond
{
    private const double GIGASECOND = 1E9;
    public static DateTime Date(DateTime dateTime)
    {
        return dateTime.AddSeconds(GIGASECOND);
    }
}