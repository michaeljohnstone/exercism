﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

internal class Binary
{
    public static int ToDecimal(string binary)
    {
        if (!Regex.IsMatch(binary.Trim(), "^[01]+$"))
        {
            return 0;
        }
        var digits = binary.Trim().ToCharArray();
        var power = binary.Length-1;

        return digits.Sum(digit => Convert.ToInt32(int.Parse(digit.ToString())*Math.Pow(2, power--)));
    }
}