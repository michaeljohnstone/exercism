﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

internal class Allergies
{
    private int _allergenScore;

    private readonly List<Tuple<string, int>> _allergens = new List<Tuple<string, int>>
    {
        Tuple.Create("cats", 128),
        Tuple.Create("pollen", 64),
        Tuple.Create("chocolate", 32),
        Tuple.Create("tomatoes", 16),
        Tuple.Create("strawberries", 8),
        Tuple.Create("shellfish", 4),
        Tuple.Create("peanuts", 2),
        Tuple.Create("eggs", 1)    
    };
    public Allergies(int allergenScore)
    {
        _allergenScore = allergenScore % (_allergens[0].Item2*2);
    }

    public bool AllergicTo(string allergen)
    {
        return List().Contains(allergen);
    }

    public List<string> List()
    {
        var remainingScore = _allergenScore;
        var allergens = new List<string> ();
        while (remainingScore > 0)
        {
            foreach (var allergen in _allergens.Where(allergen => allergen.Item2 <= remainingScore))
            {
                allergens.Add(allergen.Item1);
                remainingScore -= allergen.Item2;
            }
        }
        allergens.Reverse();
        return allergens;
    }
}