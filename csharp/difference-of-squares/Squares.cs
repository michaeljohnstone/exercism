﻿using System;

internal class Squares
{
    private int _number;

    public Squares(int number)
    {
        if (number < 0)
        {
            throw new ArgumentException("Number should be greater or equal to zero.");
        }
        _number = number;
    }

    public int SquareOfSums()
    {
        var count = 0;
        for (var i = 1; i <= _number; i ++)
        {
            count += i;
        }
        return count*count;
    }

    public int SumOfSquares()
    {
        var count = 0;
        for (var i = 1; i <= _number; i++)
        {
            count += i*i;
        }
        return count;
    }

    public object DifferenceOfSquares()
    {
        return SquareOfSums() - SumOfSquares();
    }
}