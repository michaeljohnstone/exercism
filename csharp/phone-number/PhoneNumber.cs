﻿internal class PhoneNumber
{
    public string Number;

    public string AreaCode;

    public PhoneNumber(string phoneNumber)
    {
        Number = phoneNumber.Replace("(", "").Replace(")", "").Replace("-", "").Replace(" ", "").Replace(".","");
        if (Number.Length == 11 && Number.StartsWith("1"))
        {
            Number = Number.Substring(1, Number.Length-1);
        }
        if (Number.Length == 11 && !Number.StartsWith("1"))
        {
            Number = "0000000000";
        }
        else if (Number.Length != 10)
        {
            Number = "0000000000";
        }
        AreaCode = Number.Substring(0, 3);
        
    }

    public override string ToString()
    {
        return "(" + AreaCode + ") " + Number.Substring(3, 3) + "-" + Number.Substring(6, 4);
    }
}