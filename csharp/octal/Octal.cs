﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

internal class Octal
{
    public static int ToDecimal(string value)
    {
        if (!Regex.IsMatch(value, "^[0-7]+$"))
        {
            return 0;
        }
        return value.Select((t, i) => int.Parse(value.Substring(i, 1)) * (int)Math.Pow(8, value.Length - i - 1)).Sum();
    }
}