﻿using System.Globalization;
using NUnit.Framework.Constraints;
using System.Collections.Generic;
using System.Linq;

internal class Sieve
{
    public static int[] Primes(int rangeLimit)
    {
        //a potentialPrime is a prime if its index is marked false at the end of the algorithm
        var potentialPrimes = new bool[rangeLimit+1];

        //mark multiples of each number as not prime
        for (var i = 2; i <= rangeLimit; i++)
        {
            for (var j = i*2; j <= rangeLimit; j += i)
            {
                potentialPrimes[j] = true;
            }
        }

        //whatevers left are primes
        var actualPrimes = new List<int>();
        for (var i = 2; i <= rangeLimit; i++)
        {
            if (!potentialPrimes[i])
            {
                actualPrimes.Add(i);
            }
        }

        return actualPrimes.ToArray();
    }
}