﻿using System.Collections.Generic;
using System.Linq;

internal class Pangram
{
    public const string Alphabet = "abcdefghijklmnopqrstuvwxyz";
    public static bool IsPangram(string input)
    {
        return Alphabet.All(character => input.ToLower().Contains(character));
    }
}