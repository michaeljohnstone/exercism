﻿using System;

namespace Exercism.grade_school
{
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework.Constraints;

    class School
    {
        public int test;

        public Dictionary<int, List<string>> Roster = new Dictionary<int, List<string>>();
        
        internal void Add(string student, int grade)
        {
            if (!Roster.ContainsKey(grade))
            {
                Roster[grade] = new List<string>();
            }
            Roster[grade].Add(student);
            Roster[grade].Sort();
            
        }

        public List<string> Grade(int i)
        {
            return Roster.ContainsKey(i) ? Roster[i] : new List<string>();
        }
    }
}
