﻿using System;

internal class Grains
{
    private const int MaxNumberOfSquares = 64;
    public static double Square(int i)
    {
        return Math.Pow(2, i-1);
    }

    public static double Total()
    {
        return Math.Pow(2, MaxNumberOfSquares) - 1;
    }
}