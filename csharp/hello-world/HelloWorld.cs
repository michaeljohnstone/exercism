﻿internal class HelloWorld
{
    public static string Hello(string phrase)
    {
        return phrase == null ? "Hello, World!" : $"Hello, {phrase}!";
    }
}