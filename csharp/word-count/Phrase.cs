﻿namespace Exercism.word_count
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text.RegularExpressions;

    class Phrase
    {
        public static Dictionary<string, int> WordCount(string word)
        {
            var result = new Dictionary<string, int>();
            word =
                word.ToLower()
                    .Replace(",", " ")
                    .Replace("''", "'")
                    .Replace(" ' ", " ")
                    .Replace(".", "")
                    .Replace(":", "")
                    .Replace("!", "")
                    .Replace("&", "")
                    .Replace("@", "")
                    .Replace("$", "")
                    .Replace("%", "")
                    .Replace("^", "")
                    .Replace("  ", " ")
                    .Replace("'l", "l")
                    .Replace("k'", "k");

            
            var words = word.Split(' ');
            foreach (var w in words.Where(w => !result.ContainsKey(w)))
            {
                result.Add(w, words.Count(x => x == w));
            }

            return result;
        }
    }
}
