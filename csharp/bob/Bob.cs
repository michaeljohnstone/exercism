﻿using System;

namespace Exercism.bob
{
    class Bob
    {
        internal static string Hey(string statement)
        {
            if (statement.ToUpper() == statement && statement.ToLower() != statement) 
                return "Whoa, chill out!";

            if (statement.TrimEnd(' ').EndsWith("?"))
                return "Sure.";

            if (statement.TrimEnd(' ') == "")
                return "Fine. Be that way!";

            return "Whatever.";
        }
    }
}
