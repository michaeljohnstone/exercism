﻿internal class PigLatin
{
    private const string _vowels = "aeiou";
    public static string Translate(string word)
    {
        if (!_vowels.Contains(word[0].ToString()))
            return word.Substring(1, word.Length - 1) + word[0] + "ay";
        return word + "ay";

    }
}