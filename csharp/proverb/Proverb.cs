﻿using System.Collections.Generic;

internal class Proverb
{
    private static readonly List<string> _lines = new List<string>()
    {
        "For want of a nail the shoe was lost.",
        "For want of a shoe the horse was lost.",
        "For want of a horse the rider was lost.",
        "For want of a rider the message was lost.",
        "For want of a message the battle was lost.",
        "For want of a battle the kingdom was lost.",
        "And all for the want of a horseshoe nail."
    };

    public static string Line(int line)
    {
        return _lines[line - 1];
    }

    public static object AllLines()
    {
        string concat = null;
        return string.Join("\n", _lines);
    }
}