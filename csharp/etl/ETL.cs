﻿namespace Exercism.etl
{
    using System.Collections.Generic;
    using System.Linq;

    class ETL
    {
        public static Dictionary<string, int> Transform(Dictionary<int, IList<string>> old)
        {
            var result = new Dictionary<string, int>();
            foreach (var kvp in old)
            {
                foreach (var value in kvp.Value)
                {
                    result.Add(value.ToLower(), kvp.Key);
                }
            }
            return result;
        }
    }
}
