﻿using System.Linq;

internal class Atbash
{
    public static string Encode(string words)
    {
        return words.ToLower().
            Where(char.IsLetterOrDigit).
            Select((n, index) =>
            {
                var v = (char.IsLetter(n) ? (char)('z' - (n - 'a')) : n).ToString();
                return (1 <= index && index%5 == 0) ? " " + v : v;
            })
            .Aggregate("", (a, b) => a + b);
    }
}