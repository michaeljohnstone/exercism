﻿using System;
using System.Collections.Generic;

internal class DNA
{
    private string dna;

    public Dictionary<char, int> NucleotideCounts { get; }

    public DNA(string dna)
    {
        this.dna = dna;
        NucleotideCounts = new Dictionary<char, int>() {{'A', 0}, {'T', 0}, {'C', 0}, {'G', 0}};
        foreach (var c in dna.ToCharArray())
        {
            NucleotideCounts[c]++;
        }
    }

    public object Count(char c)
    {
        if (NucleotideCounts.ContainsKey(c))
            return NucleotideCounts[c];
        throw new InvalidNucleotideException();
    }
}

internal class InvalidNucleotideException : Exception
{
}