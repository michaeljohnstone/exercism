﻿using System;
using System.Collections.Generic;

public static class Extensions
{
    public static IEnumerable<T> Accumulate<T>(this IEnumerable<T> enumerable, Func<T, T> function)
    {
        foreach (var item in enumerable)
        {
            yield return function(item);
        }
    }

    
}
