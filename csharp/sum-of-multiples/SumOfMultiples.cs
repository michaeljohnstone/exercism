﻿using System.Collections.Generic;
using System.Linq;

internal class SumOfMultiples
{
    public static int To(int[] ints, int sumTo)
    {
        return Enumerable.Range(0, sumTo).Where(x => ints.Any(xx => x%xx == 0)).Sum();
    }
}