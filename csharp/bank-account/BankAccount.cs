﻿using System;

internal class BankAccount
{
    private bool _isOpen;

    private int _balance;

    private readonly object _locked = new object();

    public void Open()
    {
        _isOpen = true;
    }

    public object GetBalance()
    {
        if (!_isOpen)
        {
            throw new InvalidOperationException();
        }
        return _balance;
    }

    public void UpdateBalance(int newBalance)
    {
        lock (_locked)
        {
            _balance += newBalance;
        }
    }

    public void Close()
    {
        _isOpen = false;
    }
}