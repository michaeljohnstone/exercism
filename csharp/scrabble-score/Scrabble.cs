﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework.Constraints;

internal class Scrabble
{
    private static Dictionary<int, char[]> letterValues = new Dictionary<int, char[]>
    {    
        {1, new [] {'a','e','i','o','u','l','n','r','s','t'}},
        {2, new [] {'d','g'}},
        {3, new [] {'b','c','m','p'}},
        {4, new [] {'f','h','v','w','y'}},
        {5, new [] {'k'}},
        {8, new [] {'j','x'}},
        {10, new[] {'q','z'} }
    };

    public static int Score(string word)
    {
        if (IsWordInvalid(word))
        {
            return 0;
        }

        return word.ToLower().ToCharArray().Sum(c => letterValues.FirstOrDefault(x => x.Value.Contains(c)).Key);
    }

    public static bool IsWordInvalid(string word)
    {
        return string.IsNullOrEmpty(word) || word.Contains("\t\n");
    }
}