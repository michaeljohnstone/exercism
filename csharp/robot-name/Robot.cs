﻿using System;

internal class Robot
{
    public string Name = string.Empty;
    public Robot()
    {
        Name = GenerateName();
    }

    public void Reset()
    {
        Name = GenerateName();
    }

    private string GenerateName()
    {
        var random = new Random(DateTime.Now.Millisecond);
        var name = Char.ConvertFromUtf32(random.Next(65, 90));
        name += Char.ConvertFromUtf32(random.Next(65, 90));
        name += random.Next(1000).ToString("D3");
        return name;
    }
}
