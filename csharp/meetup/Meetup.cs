﻿using System;
using System.Linq;

internal class Meetup
{
    private int month;
    private int year;

    public Meetup(int month, int year)
    {
        this.month = month;
        this.year =year;
    }

    internal DateTime Day(DayOfWeek dayOfWeek, Schedule schedule)
    {
        switch (schedule)
        {
            case Schedule.Teenth:
                return GetTeenthDayOfMonth(dayOfWeek);
            case Schedule.Last:
                return GetLastDayOfMonth(dayOfWeek);
            default:
                return GetXDayOfMonth(dayOfWeek, schedule);

        }
    }

    private DateTime GetXDayOfMonth(DayOfWeek dayOfWeek, Schedule x)
    {
        var firstDay = new DateTime(year, month, 1).DayOfWeek;
        var offset = dayOfWeek - firstDay + 1;
        if (offset <= 0)
        {
            offset += 7;
        }
        return new DateTime(year, month, offset + (((int)x - 1) * 7));
    }

    private DateTime GetLastDayOfMonth(DayOfWeek dayOfWeek)
    {
        var lastDate = new DateTime(year, month, 1).AddMonths(1).AddDays(-1);
        var lastDay = lastDate.DayOfWeek;
        var offset = lastDay - dayOfWeek;
        if (offset < 0)
        {
            offset += 7;
        }
        return lastDate.AddDays(-1*offset);
    }

    private DateTime GetTeenthDayOfMonth(DayOfWeek dayOfWeek)
    {
        var firstDay = new DateTime(year, month, 13).DayOfWeek;
        var offset = dayOfWeek - firstDay ;
        if (offset < 0)
        {
            offset += 7;
        }
        return new DateTime(year, month, offset + 13);
    }
}

public enum Schedule
{
    Teenth = 0,
    First = 1,
    Second = 2,
    Third = 3,
    Fourth = 4,
    Last
}